# readme

C3ISP mission is to define a collaborative and confidential information sharing, 
analysis and protection framework as a service for cyber security management. 
C3ISP innovation is the possibility to share information in a flexible and 
controllable manner inside a collaborative multi-domain environment to improve 
detection of cyber threats and response capabilities, still preserving the 
confidentiality of the shared information. C3ISP paradigm is collect, analyse, 
inform, and react. For more information, please visit the official C3ISP web site https://c3isp.eu/


**1.1 Pre-requisites**
The deployment of the C3ISP subsystems (ISI, IAI, DSA Manager, CSS) depends on the
availability of hardware (or virtual hardware) and third-party software that must be installed. It
is critical to install in advance all the pre-requisites to have a working C3ISP Framework.
The versions reported in the next tables are those tested. Generally, we do not foresee
incompatibilities with newer versions and we have also done some tests about that (e.g. we
tested on Ubuntu 18.04 LTS, Tomcat 8.5 and both Open JDK/Oracle JDK).

| Subsystem | Tested hardware | Operating System |
| ------ | ------ | ------ |
| ISI | 6 vCPU, 16 GB RAM, 100 GB HDD |  Linux Ubuntu 16.04 LTS   |
| IAI | 8 vCPU, 16 GB RAM, 200 GB HDD |  Linux Ubuntu 16.04 LTS   |
| DSA Manager   |   2 vCPU, 4 GB RAM, 40 GB HDD   |  Linux Ubuntu 16.04 LTS   |
| CSS   |   6 vCPU, 11 GB RAM, 20 GB HDD   |    Linux Ubuntu 16.04 LTS    |
| Local ISI  |   6 vCPU, 16 GB RAM, 20 GB HDD   |    Any Linux-based OS running Docker |

Local ISI instances run on Pilot specific VMs in Docker containers,
including both C3ISP components and third-party software (e.g. MySQL).

**ISI Software requirements**

| Third party software | Version | Description | 
| ------ | ------ | ------ |
| Java JRE | 1.8.0_201| Java runtime engine |
| Apache Tomcat | 8.0.32 | Java server to run C3ISP ISI components |
| MySQL | 5.7.27 | Database for continuous authorisation sessions |
| NodeJS | 8.16.1 | JavaScript engine to run Format Adapter |
| npm | 5.6.0 | Node Package Manager (for Format Adapter) |
| PM2 | 2.10.4 | Process Manager (for Format Adapter) |
| Apache Hadoop HDFS | 3.0.0 | DPOS |
| Mongo DB | 3.2.22 | Part of DPOS for DPOS metadata |

**IAI Software requirements**

| Third party software | Version | Description | 
| ------ | ------ | ------ |
| Java JRE | 1.8.0_201| Java runtime engine |
| Apache Tomcat | 8.0.32 | Java server to run C3ISP IAI components |
| MySQL | 5.7.27 | Internal DB for DGA; also act as VDL |
| Apache Hadoop HDFS | 3.0.0 | Virtual Data Lake (VDL) |
| Cingulata | N/A | Cingulata compiler toolchain | 
| Python | 2.7.12 | Python (used by some analytics) | 

**DSA Manager Software requirements**

| Third party software | Version | Description | 
| ------ | ------ | ------ |
| Java JRE | 1.8.0_201| Java runtime engine |
| Apache Tomcat | 8.0.32 | Java server to run C3ISP DSA Manager components |
| Mongo DB | 3.2.22 | DSA Store |
| MySQL | 5.7.27 | Profile Store for DSA Editor |

**CSS Software requirements**

| Third party software | Version | Description | 
| ------ | ------ | ------ |
| Java JRE | 1.8.0_201| Java runtime engine |
| Apache Tomcat | 8.0.32 | Java server to run C3ISP CSS components |
| HashiCorp Vault | 1.1.3 | Key manager service |
| MySQL | 5.7.27 | Backend for Vault |
| OpenLDAP | 2.4.42 | Identity Manager service |
| Cingulata | N/A | Cingulata compiler toolchain | 
| Graylog | 2.5.1 | Secure Audit Manager |
| MITREid Connect | 1.3.1 | OIDC Server |

**Local ISI Software requirements**

| Third party software | Version | Description | 
| ------ | ------ | ------ |
| Docker | 19.03 | Containers for ISI third-party software |
| Docker Compose | 1.24.0 | Tool to combine multiple docker containters |

**1.2 Installation procedures**
The installation procedures, described in the next sections, are presented in terms of subsystems.
Each subsystem, including Local ISI instances, should run on its own VM or physical hardware.

For each subsystem, you need to download the repository:<br>
`$ git clone https://gitlab.com/c3isp/<subgroup>/<component>.git`<br>

In order to configure the components you need to edit the application.properties
and/or application.yml files located in <component>/src/main/resources in which you will find 
generic fields that need to be filled with your system informations.

You can build each component with the following commands:
`$ cd <compoment_directory>`<br>
`$ mvn clean install -DskipTests`<br>
In this way you obtain the .war files for Tomcat.<br>

It is recommended (but not mandatory) to use the ConfigServer in order to let your components 
downloading automatically up-to-date properties files. What happens is that at bootstrap each component
looks for its own application.properties file in a specific repository. You need to create this repository
containing a directory for each subsystem (ISI, IAI, CSS, DSA-manager) with inside an 
application.properties file for each component. 
To tell your components to fetch the right application.properties you need to edit/create a 
bootstrap.properties file of the component located in the resources directory, like the following:

`# Name of the Spring Boot application`<br>
`# It will look for its name .properties or .yml on the Config Server`<br>
`# Should be unique in the Config Server`<br>
`spring.application.name=<component-name>`<br>
`spring.jmx.default-domain=<component-name>`<br>
`#spring.profiles.active=<profile-name>`<br><br>

`# Config Server URI, default is http://localhost:8888`<br>
`spring.cloud.config.uri=http://<configServer-uri>`<br><br>

`# Auth to Config Server`<br>
`spring.cloud.config.username=<configServer_username>`<br>
`spring.cloud.config.password=<configServer_password>`<br><br>

You can set username, password and uri editing the application.yml file located 
in the configServer repository (https://gitlab.com/c3isp/others/config-server/-/blob/master/src/main/resources/application.yml).

Summing up:
- create/modify a bootstrap.properties file in each component located in src/main/resources and containing the code above
- create a repository containing all the application.properties of your components
- set up the application.yml located in https://gitlab.com/c3isp/others/config-server/-/blob/master/src/main/resources/application.yml


**Information Sharing Infrastructure (ISI)**
Most of the ISI released components’ software artefacts runs on top of Apache Tomcat, which
needs to be installed and running before proceeding with the ISI installation.
The following ISI components are released as source code; in order to produce the .war files
they need to be built as explained above; installation is
straightforward and requires to upload the war files onto Tomcat. The Tomcat version shipped
with Ubuntu Linux requires the war files to be copied to /var/lib/tomcat8/webapps.
Moreover, you need to configure a network file system (nfs) between ISI and IAI machines.

ISI components (1)

| Component | Path and file |
| ------ | ------ |
| DSA Adapter Front End | /var/lib/tomcat8/webapps/dsa-adapter-frontend.war |
| Event Handler | /var/lib/tomcat8/webapps/event-handler.war | 
| Continuous Authorization Engine | /var/lib/tomcat8/webapps/multi-resource-handler.war<br>/var/lib/tomcat8/webapps/UsageControlFramework.war |
| Bundle Manager | /var/lib/tomcat8/webapps/bundle-manager.war |
| Buffer Manager | /var/lib/tomcat8/webapps/buffer-manager.war |
| Data Protected Object Storageell | /var/lib/tomcat8/webapps/dpos-api.war |
| ISI API | /var/lib/tomcat8/webapps/isi-api.war |

ISI components (2)

The following ISI components use different techniques to run. Format Adapter runs on a
NodeJS. It is required to deploy Format Adapter on top of NodeJS via the Node Package
Manager (npm, already included in NodeJS distribution); we use PM2 to run it (Process
Manager 2, a NodeJS application runner). The Config Server distributes configuration settings
to all the C3ISP components (stored in GIT repositories) and runs on
top of a standalone Java server. A running instance of the Config Server is required for each
C3ISP subsystem (ISI, IAI, DSA Manager, and CSS).

Format Adapter installation:

Install NodeJS:<br>
`$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash –`<br>
`$ sudo apt-get install nodejs`<br>
`$ sudo apt-get install build-essential # used for compiling nodejs mods`<br><br>
Create a dedicated nodejs system user via standard Linux commands.<br><br>
Login as nodejs user, then: <br>
`$ sudo npm install -g pm2 # install PM2`<br>
`$ sudo pm2 startup system # enable PM2 at OS boot-time`<br><br>
Un-tar ISI/format-adapter.tgz under /home/nodejs/format-adapter/ directory (create it first and change to it):<br>
`$ tar xzf format-adapter.tgz`<br><br>
Install Format Adapter via npm:<br>
`$ npm install`<br><br>
Start Format Adapter:<br>
`$ sudo pm2 start converter.js`<br><br>
Save configuration for future restarts:<br>
`$ pm2 save`<br>


Config Server installation:

Copy Config Server to the following location, with correct ownership:<br>
`$ sudo mkdir /opt/ConfigServer`<br>
`$ sudo cp -p <pathTo>/ConfigServer.jar /opt/ConfigServer/`<br>
`$ sudo chown -R tomcat8:tomcat8 /opt/ConfigServer`<br><br>
To enable automatic startup at boot-time via Systemd Linux daemon, create the
following file in /etc/systemd/system/ConfigServer.service :<br>
`[Unit]`<br>
`Description=ConfigServer`<br>
`After=syslog.target`<br>
`Config Server`<br><br>
`[Service]`<br>
`User=tomcat8`<br>
`ExecStart=/opt/ConfigServer/ConfigServer.jar`<br>
`SuccessExitStatus=143`<br><br>
`[Install]`<br><br>
`WantedBy=multi-user.target`<br><br>
Enable the Systemd configuration and start the Config Server:<br>
`$ sudo systemctl enable ConfigServer.service`<br>
`$ sudo systemctl start ConfigServer`<br>


**Local Information Sharing Infrastructure (Local ISI)**

Local ISI runs the software artefacts of the ISI components in Docker containers to implement
the Hybrid C3ISP deployment model. One of the advantages of the
container technology is the ease of installation and its reproducibility. There are only two
requirements for Local ISI installation:

*  Install Docker Community Edition (CE), as described in the official Docker CE guide:
e.g. for Ubuntu Linux follow https://docs.docker.com/install/linux/docker-ce/ubuntu/

Docker CE is the technology that will run the Local ISI containers under Linux

* Install Docker Compose, as reported in the official Docker Compose guide:
https://docs.docker.com/compose/install/

Docker Compose is the tool used to organise and run multi-containers applications. Local ISI
is a composition of containers that runs as a single application thanks to Docker Compose.
We prepared Docker images for all the C3ISP components part of ISI. We
also prepared a script that handles the operation of containers (initialisation, start-up, shutdown,
log inspection, etc.).
Local ISI installation procedure is the following.
Create the required directory layout on the host node (the physical or virtual machine where
you want to run the Local ISI):

`$ mkdir -p ~/C3ISP-Local-ISI/opt/c3isp`<br>
`$ sudo ln -s ~/C3ISP-Local-ISI/opt/c3isp /opt/`<br>
`$ cd ~/C3ISP-Local-ISI/`<br>
`$ tar xvzf <path-to>/C3ISP-Local-ISI_yyyymmdd-hhmmss.tar.gz`<br>
Import locally all the Docker images:<br>
`$ sudo docker load < <path-to>/<component-name>-yyyymmdd.tar.gz`<br>
where <component-name>-yyyymmdd.tar.gz is an image file corresponding to an ISI component.
Repeat tbe process for each component.

You must then load a TLS digital certificate for HTTPS communication. We have tested it with
Let’s Encrypt free certificates, but it will work with all valid ones (e.g. released by a well-
known Certification Authority). If Let’s Encrypt certificate is already available on the server in
its default location (/etc/letsencrypt), we provide a simple script to use them:<br>
`$ cd cert`<br>
`$ ./update.sh`<br>
The certificate is used for exposing the ISI API endpoint.

You can start the Local ISI instance with:<br>
`$ cd ../compose`<br>
`$ ./c3isp-isi.sh start`<br>

The Local ISI’s Swagger interface to test the ISI API will be available at:
**https://<host_fqdn>:443/isi-api/swagger-ui.html**

Note: by default, Local ISI is configured to use the C3ISP Testbed. To reconfigure for another
environment, please update the following file */opt/c3isp/Docker/ISI/compose/.env* , with
the correct profile:<br>
`# Version and profile`<br>
`C3ISP_VERSION=20190701`<br>
`SPRING_PROFILES_ACTIVE=genericLocalIsi`<br>
This will instruct the Local ISI components to fetch the settings for the corresponding C3ISP
Framework execution environment. If necessary, new C3ISP Framework execution environments
can be created by defining new C3ISP components settings in Git repositories (Git repositories
are used to store the C3ISP components configuration settings to be used at runtime).


**Information Analytics Infrastructure (IAI)**

Most of the IAI released components’ software artefacts run on top of Apache Tomcat, which
needs to be installed and running before proceeding with the IAI installation.
The following IAI components are released as source code; in order to produce the .war files
they need to be built as explained above; installation is
straightforward and requires to upload the war files onto Tomcat. The Tomcat version shipped
with Ubuntu Linux requires the war files to be copied to /var/lib/tomcat8/webapps.
Moreover, you need to configure a network file system (nfs) between ISI and IAI machines.

| Component | Path and file |
| ------ | ------ |
| IAI API | /var/lib/tomcat8/webapps/iai-api.war |
| FHE Analytics | /var/lib/tomcat8/webapps/fhe-conn-malicious-host.war |
| C3isp Analytic Engine | /var/lib/tomcat8/webapps/monitoring-dga.war |
| C3isp Analytic Engine | /var/lib/tomcat8/webapps/DrillController.war |
| C3isp Analytic Engine | /var/lib/tomcat8/webapps/loganalytic.war |
| C3isp Analytic Engine | /var/lib/tomcat8/webapps/sme-analytic.war |
| C3isp Analytic Engine | /var/lib/tomcat8/webapps/threat-analyzer.war |
| Service Usage Control Adapter | /var/lib/tomcat8/webapps/UsageControlFramework.war |
| Service Usage Control Adapter | /var/lib/tomcat8/webapps/pap-api.war |

IAI requires a Config Server to be installed before starting it. Please follow the same procedure
described for the ISI


**DSA Manager**

All DSA Manager released components’ software artefacts run on top of Apache Tomcat, which
needs to be installed and running before proceeding with the DSA Manager installation.
The following DSA Manager components are released source code; in order to produce the .war files
they need to be built as explained above;
installation is straightforward and requires to upload the war files onto Tomcat. The Tomcat
version shipped with Ubuntu Linux requires the war files to be copied to
/var/lib/tomcat8/webapps.

| Component | Path and file |
| ------ | ------ |
| Dsa Editor | /var/lib/tomcat8/webapps/DSAEditor.war |
| Dsa Editor | /var/lib/tomcat8/webapps/DSAAuthoringTool.war |
| Dsa Mapper | /var/lib/tomcat8/webapps/dsa-mapper.war |
| DSA Store API | /var/lib/tomcat8/webapps/dsa-store-api.war |

DSA Manager requires a Config Server to be installed before starting it. Please follow the same
procedure described for the ISI.


**Common Security Services (CSS)**

All CSS released components’ software artefacts run on top of Apache Tomcat, which needs to
be installed and running before proceeding with the installation.
The following components are released as source code; in order to produce the .war files
they need to be built as explained above; installation is
straightforward and requires to upload the war files onto Tomcat. The Tomcat version shipped
with Ubuntu Linux requires the war files to be copied to /var/lib/tomcat8/webapps.

| Component | Path and file |
| ------ | ------ |
| Key and Encryption Manager | /var/lib/tomcat8/webapps/ke-core-manager-api.war |
| Key and Encryption Manager | /var/lib/tomcat8/webapps/dpos-key.war |
| Key and Encryption Manager | /var/lib/tomcat8/webapps/dpos-encryption.war |
| Key and Encryption Manager | /var/lib/tomcat8/webapps/fhe-key.war |
| Key and Encryption Manager | /var/lib/tomcat8/webapps/fhe-encryption.war |
| Secure Audit Manager API  | /var/lib/tomcat8/webapps/audit-logger.war |
| Secure Audit Manager API | /var/lib/tomcat8/webapps/audit-logging-filter.war |

CSS requires a Config Server to be installed before starting it. Please follow the same
procedure described for the ISI.


**Operational procedures**

This section describes how the installed C3ISP Framework can be used/tested and how it can
be integrated into/used by an application. To support these needs, the subsystems of the C3ISP
Framework provide external APIs based on the RESTful web services paradigm, where 
there is a list of HTTP endpoints that can be called programmatically. 
To exploit the C3ISP Framework functionalities only the external APIs are
requested; it is not necessary to call APIs of internal components. In fact, the Local ISI shields
all the internal components and exposes only the external APIs, thanks to the containers
isolation.
To make testing and integration easier, developers/testers can call the APIs through a simple
web interface that describes their signatures and allows specifying the required parameters
(these are based on Swagger Open API).
The C3ISP Pilots application integrates with the C3ISP Framework via these external APIs.
They are the following:

| Subsystem | Component | How | Where |
| ------ | ------ | ------ | ------ |
| ISI (Including Local ISI) | ISI API | RESTful webservices | https://your_domain/isi-api/swagger-ui.html |
| IAI | IAI API | RESTful webservices | https://your_domain/iai-api/swagger-ui.html |
| DSA Manager | DSA Editor | Web interface | https://your_domain/DSAEditor/ |


**Databases creation guide**
You need to create, for each subsystem, the following databases: <br>
- ISI: c3isp, datalakebuffer, dsaAdapter, session_manager, dpostore (MongoDB)
- IAI: c3isp, datalakebuffer, policyadministrationpoint, session_manager, dga_list
- CSS: keyDB 
- DSAMGR: dsarepo, dsastore (MongoDB) 

All the previous are MySql dbs except for dpostore (in DSAMGR) and dsastore (in ISI) that are in MongoDB. <br>

Some of those DBs need to be updated by hand. In particular:
- "c3isp" db in IAI and ISI must be the same. They contain a table called "users" that needs to be filled up with users attributes. 
This is usefull for the ServiceUsageControlAdapter and DataUsageControlAdapter for monitoring the user's attributes
- "policyadministrationpoint" keeps inside policies used by the ServiceUsageControlAdapter. You can add a new policy by using the APIs from the swagger
